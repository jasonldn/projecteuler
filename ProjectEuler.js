const ProjectEuler = (() => {
  "use strict";

  const isPrime = (x) => {

    if (x === 0 || x === 1)
      return false;
    else if (x === 2)
      return true;
    else {
      if (x % 2 == 0) {
        return false;
      }

      for (let i = 3, end = Math.sqrt(x); i <= end; i += 2) {
        if (x % i === 0) {
          return false;
        }
      }
      return true;
    }
  };

  const firstNPrimes = (n) => {
    let result = [];
    let num = 2;
    let count = 1;

    while (count <= n) {
      if (isPrime(num)) {
        result.push(num);
        count++;
      }
      num++;
    }

    return result;
  };

  /*
   Let S(n) be the sum of all contiguous integer sub-strings that can be formed from the integer n.  The sub-strings need not be distinct.
   For example, S(2024) = 2 + 0 + 2 + 4 + 20 + 02 + 24 + 202 + 024 + 2024 = 2304.

   Let P(n) be the integer formed by concatenating the first n primes together.
   For example, P(7) = 2357111317.

   Let C(n,k) be the integer formed by concatenating k copies of P(n) together.
   For example, C(7,3) = 235711131723571113172357111317.

   Evaluate S( C(10 ** 6, 10 ** 12) ) mod (10 ** 9 + 7).
   */
  const p603 = () => {
    console.time('p603');
    let S = (strNum) => {

      let strLen = strNum.length;
      let sum = 0;
      let endMultiplier = 0;

      for (let i = strLen - 1; i >= 0; i--) {
        endMultiplier = 10 * endMultiplier + 1;
        sum += strNum[i] * (i + 1) * endMultiplier;
      }

      console.log("sum : " + sum);
      return sum;
    };

    let P = (n) => {

      let primes = firstNPrimes(n);
      return primes.join("");
    };

    let C = (n, k) => {

      let oneCopy = P(n);
      let copies = [];

      for (let i = 1; i <= k; i++) {
        copies.push(oneCopy);
      }

      return copies.join('');
    };

    console.timeEnd('p603');
    return S( C( Math.pow(10, 6), Math.pow(10, 12) ) ) % ( Math.pow(10, 9) + 7 );
    //C( Math.pow(10, 6), 3 );
    //return P(Math.pow(10, 6));
    //return S( C(7,3) );
    //return S( "2024" );
  };

  // How many integers 0 ≤ n < 10**18 have the property that the sum of the digits of n equals the sum of digits of 137n?
  const p290 = () => {
    console.time('p290');

    const key = 137;
    let limit = Math.pow(10, 8),
        counter = 0;

    let sum;
    for (let j = limit - 1; j >= 10000000; j-=9) {

      if (j % 9 === 0) {
        sum = sumOfDigits(key * j);
        if (sumOfDigits(j) === sum) {
          counter += 1;
        }
      }
    }

    // 99         -> 0          : 4
    // 999        -> 100        : 29
    // 9999       -> 1000       : 273
    // 99999      -> 10000      : 2596
    // 999999     -> 100000     : 25053
    // 9999999    -> 1000000    : 243568
    // 99999999   -> 10000000   : 2364871

    console.timeEnd('p290');
    console.log('*** count : ' + counter);
    return counter;
  };

  const trackSeq = (seq, tracker) => {
    let number = parseInt(seq, 2);
    tracker[number] = 1;
  };

  const hasOccurred = (seq, tracker) => {

    let number = parseInt(seq, 2);
    return tracker[number];
  };

  /*
   2^N binary digits can be placed in a circle so that all the N-digit clockwise subsequences are distinct.
   For N=3, two such circular arrangements are possible, ignoring rotations:
      00010111
      00011101

   For the first arrangement, the 3-digit subsequences, in clockwise order, are:
   000, 001, 010, 101, 011, 111, 110 and 100.

   Each circular arrangement can be encoded as a number by concatenating the binary digits starting with the subsequence
   of all zeros as the most significant bits and proceeding clockwise. The two arrangements for N=3 are thus represented as 23 and 29:

   00010111 2 = 23
   00011101 2 = 29
   Calling S(N) the sum of the unique numeric representations, we can see that S(3) = 23 + 29 = 52.

   Find S(5).
   */
  const p265 = () => {
    // starts with "00000", "00001" ...
    // last digit of the circle has to be "1" to give the last sub-sequence of "10000"; otherwise will collide with 1st sub-sequence of "00000".
    // thus the circle is of the form "000001xxxxxxxxxxxxxxxxxxxxxxxxx1"
    console.time('p265');
    const N = 5;
    let subseq1  = "00000",
        subseq2  = "00001",
        subseq31 = "10000",   // 16
        tracker  = new Array(N),
        pathSeen = {};

    trackSeq(subseq1, tracker);
    trackSeq(subseq2, tracker);
    trackSeq(subseq31, tracker);

    let limit = Math.pow(2, N),   // ES6 has exponential operator ('**') or (1 << n)
        currentSeq = "00001",
        head = "",
        body = "";

    for (let i = 2; i < limit; i++) {
      head = currentSeq[0];
      body = currentSeq.slice(1);

      if (!hasOccurred(body+head, tracker)) {
        currentSeq = body+head;
        trackSeq(currentSeq, tracker);
      } else {

      }
    }

    console.timeEnd('p265');
  };

  const gcd = (n1, n2) => {

    //base case
    if (n2 == 0) {
      return n1;
    }

    return gcd(n2, n1 % n2);
  };

  const checkPerfectSquareNumber = (n) => {
  //  http://burningmath.blogspot.com/2013/09/how-to-check-if-number-is-perfect-square.html
  //  a number is NOT a perfect square if:
  //  - it ends in 2, 3, 7 or 8
  //  - it terminates in an odd number of zeros
  //  - its last digit is 6 but its tens-digit is even
  //  - its last digit is not 6 but its tens-digit is odd
  //  - its last digit is 5 but its tens-digit is other than 2
  //  - its last 2 digits are not divisible by 4 if it is even number

  //  let numStr = n.toString();
  //  let strlen = numStr.length;
  //
  //  switch (numStr[strlen - 1]) {
  //    case '2':
  //    case '3':
  //    case '7':
  //    case '8':
  //      return false;
  //      break;
  //    default:
  //
  //  }

    let x = 1;
    for (let k = 0; k < n; k++) {
      x = (0.5 * (x + n / x));
    }

    return (n % x == 0);
  };

  const checkPerfectTriangle = (a, b, c) => {
    if (gcd(a,b) === 1 && gcd(b,c) === 1) {
      // check if c is perfect square
    }
  };

  const multipleOf6and8 = (a, b) => {
    let area = a * b;
    if (area % 6 === 0 && area % 28 === 0) {
      return true;
    }
  };

  /*
   Consider the right angled triangle with sides a=7, b=24 and c=25. The area of this triangle is 84, which is divisible
   by the perfect numbers 6 and 28.  Moreover it is a primitive right angled triangle if gcd(a,b)=1 and gcd(b,c)=1.
   Also c is a perfect square.

   We will call a right angled triangle perfect if
   -it is a primitive right angled triangle
   -its hypotenuse is a perfect square

   We will call a right angled triangle super-perfect if
   -it is a perfect right angled triangle and
   -its area is a multiple of the perfect numbers 6 and 28.

   How many perfect right-angled triangles with c ≤ 10**16 exist that are not super-perfect?
   */
  const p218 = () => {
    console.time('p218');
    let limit = Math.pow(10, 16);
    let counter = 0;

    for (let i = limit; i > 1; i--) {
      if (checkPerfectSquareNumber(i)) {
        // need to find a & b
        counter++;
      }
    }

    console.timeEnd('p218');
    return counter;
  };

  const fibonacci = (number) => {

    if (number < 0) {
      // "Invalid argument for Fibonacci series"
      return;
    }

    switch (number) {
      case 0:
        return 0;
      case 1:
      case 2:
        return 1;
      default:
        let fibo1 = 1, fibo2 = 1, fibonacci = 1;

        for (let i = 3; i <= number; i++) {
        fibonacci = fibo1 + fibo2;
        fibo1 = fibo2;
        fibo2 = fibonacci;
      }

        return fibonacci;
    }
  };

  /*
   The Fibonacci sequence is defined by the recurrence relation:
   Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.

   It turns out that F541, which contains 113 digits, is the first Fibonacci number for which the last nine digits are
   1-9 pan-digital (contain all the digits 1 to 9, but not necessarily in order). And F2749, which contains 575 digits,
   is the first Fibonacci number for which the first nine digits are 1-9 pan-digital.

   Given that Fk is the first Fibonacci number for which the first nine digits AND the last nine digits are 1-9 pan-digital, find k.
   */
  const p104 = () => {
    console.time('p104');
    console.log("fib: " + fibonacci(2749).toString());

    console.timeEnd('p104');
  };


  const sumOfDigits = (number) => {

    if (number < 10) {
      return number;
    }

    let sum = 0;
    let numStr = number.toString();
    let idx = numStr.indexOf("e");

    if (idx != -1) {
      for (let i = 0; i < idx; i++) {
        if (numStr[i] !== "0" && numStr[i] !== ".") {
          sum += parseInt(numStr[i]);
        }
      }
    } else {
      for (let i = 0; i < numStr.length - 1; i++) {
        if (numStr[i] !== "0") {
          sum += parseInt(numStr[i]);
        }
      }
    }

    return sum;
  };

  /*
   1**1 .. 1**99  :  max sum is always 1
   2**1=2, 3**1=3, 4**1=4 .. 99**1=99  : max sum is 18
   Therefore, we can set initial max to 18.
   */
  const p56 = () => {
    console.time('p56');
    let limit = 99;
    let max = 18;
    let number = 0;

    // process the diagonal
    for (let i = limit; i >= 2; i--) {
      number = sumOfDigits(Math.pow(i, i));

      if (number > max) {
        max = number;
      }
    }

    // only need to process half of the matrix
    let n1, n2;
    for (let row = limit; row >= 3; row--) {
      for (let col = row - 1; col >= 2; col--) {
        n1 = sumOfDigits(Math.pow(row, col));
        n2 = sumOfDigits(Math.pow(col, row));
        number = Math.max(n1, n2);

        if (number > max) {
          max = number;
        }
      }
    }
    console.log("max digit sum: " + max);
    console.timeEnd('p56');
    return max;
  };

  return {
    p603: p603,
    p290: p290,
    p265: p265,
    p218: p218,
    p104: p104,
    p56:  p56
  };
})();

//ProjectEuler.p603();   // in progress - out of memory for large string
ProjectEuler.p290();     // takes too long after 10**8
//ProjectEuler.p265();
//ProjectEuler.p218();
//ProjectEuler.p104();
ProjectEuler.p56();      // done
